<html>
  <head>
    <title>Отправка формы</title>
    <meta charset="utf-8"> 
    <link rel="stylesheet" type="text/css" href="styles.css">
  </head>
  <body>  	
  	<div class="form-wrapper">
  	  <label for="color_orange"></label>
      <input type="radio" name="dva" id="color_orange" value="color_orange" checked/> 
      <label for="color_blue"></label>
      <input type="radio" name="dva" id="color_blue" value="color_blue"/> 
      <label for="color_green"></label>
      <input type="radio" name="dva" id="color_green" value="color_green"/> 	
  	  <form action="get.php" method="POST" id="form">  	  
  	    <input type="email" name="email" placeholder="Адресс почты" required/>   	  
        <input type="text" name="name" placeholder="Имя" required/>            
        <textarea name="message" placeholder="Сообщение" accept-charset="utf-8" required></textarea> 
        <input type="submit"/>
        <div class="test-types">
      	  <label for="editing">Анкета</label>
      	  <input type="checkbox" name="edit" id="editing"/>
      	  <div class="edit">
      	    <div class="about">Вам понравилась данная форма?<br>
      	      <input type="radio" name="like" id="like_y" value="Да"/>
              <label for="like_y">Да</label><br>
	            <input type="radio" name="like" id="like_n" value="Нет"/>
              <label for="like_n">Нет</label><br>
      	    </div>      	    
	          <div class="about">Удобно ли пользоваться данной формой?<br>
      	      <input type="radio" name="useful" id="useful_y" value="Да"/>
              <label for="useful_y">Да</label><br>
	            <input type="radio" name="useful" id="useful_n" value="Нет"/>
              <label for="useful_n">Нет</label><br>
      	    </div>    
      	    <div class="about">Заметили ли вы меню выбора цвета сверху?<br>
      	      <input type="radio" name="color" id="color_y" value="Да"/>	
              <label for="color_y">Да</label><br>
	            <input type="radio" name="color" id="color_n" value="Нет"/>
              <label for="color_n">Нет</label><br>
      	    </div>    
      	    <div class="about">Устраивает ли вас предоставленная цветовая палитра?<br>
      	      <input type="radio" name="likecolor" id="likecolor_y" value="Да"/>
              <label for="likecolor_y">Да</label><br>
	            <input type="radio" name="likecolor" id="likecolor_n" value="Нет"/>
              <label for="likecolor_n">Нет</label><br>             
      	    </div>   
            <div class="about">Ваши любимые цвета?<br>
              <input type="checkbox" name="favorite[]" id="fvrt_red" value="красный">  
              <label for="fvrt_red">красный</label>
              <input type="checkbox" name="favorite[]" id="fvrt_green" value="зеленый"> 
              <label for="fvrt_green">зеленый</label>
              <input type="checkbox" name="favorite[]" id="fvrt_blue" value="синий">  
              <label for="fvrt_blue">синий</label>    
            </div>  

          </div>      	  
        </div> 
        <input type="hidden" name="info" value="<?php print(date("Y-m-d, H:i:s")); ?>"/> 
        <input type="hidden" name="hesh" value="2a9759e7af927b36f11bd6f0c8391404"/>
      </form>        
  	</div>  	
  </body>
</html>